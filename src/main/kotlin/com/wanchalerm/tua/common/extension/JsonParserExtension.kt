package com.wanchalerm.tua.common.extension

import com.fasterxml.jackson.core.JsonParser
import com.wanchalerm.tua.common.constant.ExceptionConstant
import com.wanchalerm.tua.common.exception.InputValidationException

fun JsonParser.throwBadRequestIfEmptyOrTextualNull() {
    if (this.text.isBlankOrTextualNull() == true) {
        throw InputValidationException(message = ExceptionConstant.FIELD_IS_INVALID.format(this.currentName()))
    }
}
