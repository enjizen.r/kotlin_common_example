package com.wanchalerm.tua.common.extension

import com.fasterxml.jackson.databind.ObjectMapper
import com.wanchalerm.tua.common.constant.MaskingKeyConstant.CITIZEN_ID
import com.wanchalerm.tua.common.constant.MaskingKeyConstant.EMAIL
import com.wanchalerm.tua.common.constant.MaskingKeyConstant.MOBILE_NUMBER
import com.wanchalerm.tua.common.constant.RegexpConstant

private val objectMapper = ObjectMapper()
private val log by LoggerDelegate()


fun String.maskJsonValue(maskingKeys: MutableList<String>): String {
    return try {
        val jsonNode = objectMapper.readTree(this)
        maskingKeys.forEach { key -> jsonNode.traverseAndMask(key) }
        objectMapper.writeValueAsString(jsonNode)
    } catch (e: Exception) {
       log.error("maskJsonValue Error", e)
        this
    }
}

fun String.hiddenJsonValue(hiddenKeys: MutableList<String>): String {
    return try {
        val jsonNode = objectMapper.readTree(this)
        hiddenKeys.forEach { key -> jsonNode.traverseAndHidden(key) }
        objectMapper.writeValueAsString(jsonNode)
    } catch (e: Exception) {
        log.error("hiddenJsonValue Error", e)
        this
    }
}


fun String.maskEmail(): String {
    val emailRegex = Regex("([^@]+)@(.+)")
    return when {
        emailRegex.matches(this) -> {
            val (localPart, domain) = emailRegex.find(this)!!.destructured
            val maskedLocalPart = when {
                localPart.length > 2 -> localPart.first() + "*".repeat(localPart.length - 2) + localPart.last()
                else -> localPart.replace(Regex("."), "*")
            }
            "$maskedLocalPart@$domain"
        }
        else -> {
            log.error("Invalid email address")
            this
        }
    }
}

fun String.maskMobileNumber(): String {
    return when {
        this.length > 4 -> "${this.take(2)}****${this.takeLast(4)}"
        else -> this
    }
}

fun String.maskMiddle(): String {
    return when {
        this.length <= 2 -> "*".repeat(this.length)
        else -> {
            val firstChar = this.first()
            val lastChar = this.last()
            val middleMask = "*".repeat(this.length - 2)
            "$firstChar$middleMask$lastChar"
        }
    }
}


fun String?.maskSensitiveData(): String? {
    return this?.let { data ->
        RegexpConstant.patterns.entries.fold(data) { maskedResult, (type, regex) ->
            regex.replace(maskedResult) { matchResult ->
                when (type) {
                    MOBILE_NUMBER -> matchResult.value.maskMobileNumber()
                    EMAIL -> matchResult.value.maskEmail()
                    CITIZEN_ID -> matchResult.value.maskCitizenId()
                    else -> matchResult.value
                }
            }
        }
    }
}

fun String.maskCitizenId() = this.replace(Regex("(\\d{2})\\d{8}(\\d{3})"), "$1********$2")




fun String.replaceFirst(firstPosition: Int = 4): String {
    return when {
        this.length >= firstPosition -> "***${substring(firstPosition)}"
        else ->  "***${substring(this.length)}"
    }
}