package com.wanchalerm.tua.common.extension

import com.google.common.hash.Hashing
import java.nio.charset.StandardCharsets
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.SecureRandom
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.Base64
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec


fun String.createAesKey() : SecretKeySpec = SecretKeySpec(Base64.getDecoder().decode(this), "AES")

fun String.createIv() = GCMParameterSpec(128, Base64.getDecoder().decode(this))

fun String.getPublicKeyFromBase64(): PublicKey {
    val publicBytes = Base64.getDecoder().decode(this)
    val keySpec = X509EncodedKeySpec(publicBytes)
    val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
    return keyFactory.generatePublic(keySpec)
}

fun String.getPrivateKeyFromBase64(): PrivateKey {
    val privateBytes = Base64.getDecoder().decode(this)
    val keySpec = PKCS8EncodedKeySpec(privateBytes)
    val keyFactory = KeyFactory.getInstance("RSA")
    return keyFactory.generatePrivate(keySpec)
}

fun generateSaltNumber() = SecureRandom().nextInt(90000) + 10000

fun String.encodeWithSalt(saltNumber: Int? = null): Pair<String?, Int?> {
    val salt = saltNumber ?: generateSaltNumber()
    val dataToHash = "$this${(this.length + salt)}"
    val encoded = Hashing.sha256()
        .hashString(dataToHash, StandardCharsets.UTF_8)
        .toString()
    return encoded to salt
}
