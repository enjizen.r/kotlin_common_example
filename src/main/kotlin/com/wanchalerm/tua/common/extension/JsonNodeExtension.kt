package com.wanchalerm.tua.common.extension

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.wanchalerm.tua.common.constant.MaskingKeyConstant.EMAIL
import com.wanchalerm.tua.common.constant.MaskingKeyConstant.FIRST_NAME
import com.wanchalerm.tua.common.constant.MaskingKeyConstant.LAST_NAME
import com.wanchalerm.tua.common.constant.MaskingKeyConstant.MOBILE_NUMBER

fun JsonNode.traverseAndMask(keyToMask: String) {
    when {
        isObject -> fields().forEachRemaining { entry ->
            when (entry.key) {
                keyToMask -> maskField(entry, keyToMask)
                else -> entry.value.traverseAndMask(keyToMask)
            }
        }
        isArray -> elements()
            .forEachRemaining { element -> element.traverseAndMask(keyToMask) }
    }
}

fun JsonNode.maskField(entry: Map.Entry<String, JsonNode>, key: String) {
    val value = entry.value.textValue() ?: ""
    val objectNode = (this as ObjectNode)
    when (key) {
        EMAIL -> objectNode.put(key, value.maskEmail())
        MOBILE_NUMBER -> objectNode.put(key, value.maskMobileNumber())
        FIRST_NAME, LAST_NAME -> objectNode.put(key, value.maskMiddle())
        else -> objectNode.put(key, value.replaceFirst())
    }
}

fun JsonNode.traverseAndHidden(keyToMask: String) {
    when {
        isObject -> {
            fields().forEachRemaining { entry ->
                when (entry.key) {
                    keyToMask -> (this as ObjectNode).put(keyToMask, "*****")
                    else -> entry.value.traverseAndMask(keyToMask)
                }
            }
        }
        isArray -> elements().forEachRemaining { element -> element.traverseAndMask(keyToMask) }
    }
}