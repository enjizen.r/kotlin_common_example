package com.wanchalerm.tua.common.extension

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.reflect.KProperty

class LoggerDelegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): Logger = LoggerFactory.getLogger(thisRef!!.javaClass)
}