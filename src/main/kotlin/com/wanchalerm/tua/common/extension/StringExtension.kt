package com.wanchalerm.tua.common.extension

import com.github.f4b6a3.uuid.UuidCreator
import com.wanchalerm.tua.common.constant.RegexpConstant
import java.util.Locale

fun String.camelToSnake() = this.replace(RegexpConstant.CAMEL_TO_SNAKE.toRegex(), "$1_$2").lowercase(Locale.getDefault())

fun String.createCorrelationId() = "$this-${UuidCreator.getTimeOrderedEpoch().toString()
                                    .replace("-", "")}".take(32)

fun String.isBlankOrTextualNull() = this.isBlank() == true || this.equals("null", ignoreCase = true)