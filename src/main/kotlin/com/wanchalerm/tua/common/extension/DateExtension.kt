package com.wanchalerm.tua.common.extension

import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date

fun Date.toLocalDateTime(): LocalDateTime? = Instant.ofEpochMilli(time)
    .atZone(ZoneId.systemDefault())
    .toLocalDateTime()

fun Date.toLocalDate(): LocalDate? = Instant.ofEpochMilli(time)
    .atZone(ZoneId.systemDefault())
    .toLocalDate()

fun LocalDate.isAvailable(startDate: LocalDate, endDate: LocalDate) = this in startDate..endDate

fun LocalDateTime.isAvailable(startDate: LocalDateTime, endDate: LocalDateTime) = this in startDate..endDate
