package com.wanchalerm.tua.common.extension

import com.wanchalerm.tua.common.constant.RegexpConstant.EMAIL_FORMAT
import com.wanchalerm.tua.common.constant.RegexpConstant.MOBILE_NUMBER
import java.util.regex.Pattern

fun String.isValidEmail(): Boolean = Pattern.matches(EMAIL_FORMAT, this)

fun String.isValidMobileNumber() = Pattern.matches(MOBILE_NUMBER, this)