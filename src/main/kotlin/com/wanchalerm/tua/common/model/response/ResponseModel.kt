package com.wanchalerm.tua.common.model.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.wanchalerm.tua.common.constant.ResponseEnum

@JsonPropertyOrder(
    "status",
    "data"
)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ResponseModel<T>(
    responseEnum: ResponseEnum = ResponseEnum.SUCCESS,
    responseStatus: ResponseStatus? = null,
    @JsonProperty("data") val data: T? = null,
) : ResponseCommon() {

    init {
        super.status = responseStatus ?: responseEnum.let {
            ResponseStatus(code = it.code, message = it.message)
        }
    }
}
