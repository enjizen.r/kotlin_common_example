package com.wanchalerm.tua.common.model.response


data class ResponseStatus (
    val code: String,
    val message: String
)
