package com.wanchalerm.tua.common.constant

object MaskingKeyConstant {
    const val EMAIL = "email"
    const val MOBILE_NUMBER = "mobile_number"
    const val FIRST_NAME = "first_name"
    const val LAST_NAME = "last_name"
    const val CITIZEN_ID = "citizen_id"
}