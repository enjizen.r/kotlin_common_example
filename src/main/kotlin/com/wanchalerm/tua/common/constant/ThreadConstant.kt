package com.wanchalerm.tua.common.constant

object ThreadConstant {
    const val X_MESSAGE = "X-Message"
    const val X_CORRELATION_ID = "X-Correlation-Id"
    const val X_DEVICE_ID = "X-Device-Id"
    const val X_CUSTOMER_CODE = "X-Customer-Code"
    const val X_CLIENT_IP = "X-Client_Id"
}