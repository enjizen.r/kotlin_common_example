package com.wanchalerm.tua.common.constant

object RegexpConstant {
    const val EMAIL_FORMAT = """\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b"""
    const val CAMEL_TO_SNAKE = "([a-z])([A-Z]+)"
    const val MOBILE_NUMBER = """\b\d{10}\b"""
    const val CITIZEN_ID = """^\d{13}$"""

    val patterns = mapOf(
        MaskingKeyConstant.MOBILE_NUMBER to MOBILE_NUMBER.toRegex(),
        MaskingKeyConstant.EMAIL to EMAIL_FORMAT.toRegex(),
        MaskingKeyConstant.CITIZEN_ID to CITIZEN_ID.toRegex()
    )
}