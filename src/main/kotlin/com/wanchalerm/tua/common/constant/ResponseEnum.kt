package com.wanchalerm.tua.common.constant

enum class ResponseEnum(val code: String, val message: String) {
    SUCCESS("2000", ResponseStatusConstant.SUCCESS),
    CREATED("2001", ResponseStatusConstant.CREATED),
    BAD_REQUEST("4000", ResponseStatusConstant.BAD_REQUEST),
    NO_CONTENT("2004", ResponseStatusConstant.NO_CONTENT),
    CONFLICT("4009", ResponseStatusConstant.CONFLICT),
    INTERNAL_SERVER_ERROR("5000", ResponseStatusConstant.INTERNAL_SERVER_ERROR),
    UNAUTHORIZED("4001", ResponseStatusConstant.UNAUTHORIZED),
    TIMEOUT("4008", ResponseStatusConstant.TIMEOUT),
    BAD_GATEWAY("5020", ResponseStatusConstant.BAD_GATEWAY),
    SERVICE_UNAVAILABLE("5030", ResponseStatusConstant.SERVICE_UNAVAILABLE);

    companion object {
        fun getByCode(code: String?) =
            entries.firstOrNull { responseEnum -> responseEnum.code == code } ?: INTERNAL_SERVER_ERROR
    }
}
