package com.wanchalerm.tua.common.service

import com.wanchalerm.tua.common.extension.createAesKey
import com.wanchalerm.tua.common.extension.createIv
import java.nio.charset.StandardCharsets
import java.util.Base64
import javax.crypto.Cipher

class AESService {

    private val cipher = Cipher.getInstance("AES/GCM/NoPadding")

    fun encrypt(data: String, key: String, iv: String): String {
        cipher.init(Cipher.ENCRYPT_MODE, key.createAesKey(), iv.createIv())
        val encryptedBytes = cipher.doFinal(data.toByteArray(StandardCharsets.UTF_8))
        return Base64.getEncoder().encodeToString(encryptedBytes)
    }

    fun decrypt(encryptedData: String, key: String, iv: String): String {
        cipher.init(Cipher.DECRYPT_MODE, key.createAesKey(), iv.createIv())
        val decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedData))
        return String(decryptedBytes, StandardCharsets.UTF_8)
    }
}
