package com.wanchalerm.tua.common.exception

import com.wanchalerm.tua.common.constant.ResponseEnum.UNAUTHORIZED


class UnauthorizedException (
    val code: String? = UNAUTHORIZED.code,
    override val message: String? = UNAUTHORIZED.message,
    throwable: Throwable? = null
) : RuntimeException(message, throwable)