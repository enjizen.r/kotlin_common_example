package com.wanchalerm.tua.common.exception

import org.springframework.http.HttpStatus

class BusinessException(
    val code: String,
    override val message: String,
    val httpStatus: HttpStatus = HttpStatus.CONFLICT,
    throwable: Throwable? = null
) : RuntimeException(message, throwable)
