package com.wanchalerm.tua.common.config.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers.IntegerDeserializer
import com.wanchalerm.tua.common.extension.throwBadRequestIfEmptyOrTextualNull
import java.io.IOException

class CustomIntegerDeserializer : JsonDeserializer<Int>() {
    @Throws(IOException::class)
    override fun deserialize(
        jsonParser: JsonParser,
        deserializationContext: DeserializationContext
    ): Int {
        jsonParser.throwBadRequestIfEmptyOrTextualNull()
        return IntegerDeserializer(Int::class.java, null).deserialize(jsonParser, deserializationContext)
    }
}