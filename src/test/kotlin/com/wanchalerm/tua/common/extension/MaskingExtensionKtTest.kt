package com.wanchalerm.tua.common.extension

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class MaskingExtensionKtTest {

    @Test
    fun `mask email`() {
        val result = "enjizen.rssssssss@gmail.com".maskEmail()
        assertEquals("e***************s@gmail.com", result)
    }

    @Test
    fun `mask Mobile Number`() {
        val result = "0919890217".maskMobileNumber()
        assertEquals("09****0217", result)
    }
}