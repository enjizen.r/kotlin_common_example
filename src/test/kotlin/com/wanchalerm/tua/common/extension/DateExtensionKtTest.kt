package com.wanchalerm.tua.common.extension

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.time.LocalDate

import java.util.Date

class DateExtensionKtTest {

    @Nested
    inner class ToLocalDateTime {
        @Test
        fun `toLocalDateTime success`() {
            val d = Date()
            val result = d.toLocalDateTime()
            assertNotNull(result)
        }

        @Test
        fun `toLocalDateTime null`() {
            val d: Date? = null
            val result = d?.toLocalDateTime()
            assertNull(result)
        }
    }

    @Nested
    inner class IsAvailable {
        @Test
        fun `isAvailable success`() {
            val startDate = LocalDate.of(2021, 1, 1)
            val endDate = LocalDate.of(2021, 12, 31)
            val d = LocalDate.of(2021, 1, 10)
            val result = d.isAvailable(startDate, endDate)
            assertTrue(result)
        }

        @Test
        fun `isAvailable start date greater than end date`() {
            val startDate = LocalDate.of(2021, 12, 31)
            val endDate = LocalDate.of(2021, 1, 1)
            val d = LocalDate.of(2021, 1, 10)
            val result = d.isAvailable(startDate, endDate)
            assertFalse(result)
        }

        @Test
        fun `isAvailable null date`() {
            val startDate = LocalDate.of(2021, 1, 1)
            val endDate = LocalDate.of(2021, 12, 31)
            val d: LocalDate? = null
            val result = d?.isAvailable(startDate, endDate) == true
            assertFalse(result)
        }
    }
}