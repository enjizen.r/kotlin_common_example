package com.wanchalerm.tua.common.extension

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class StringExtensionKtTest {

    @Test
    fun camelToSnake() {
        val result = "citizenId".camelToSnake()
        assertEquals("citizen_id", result)
    }

    @Test
    fun createCorrelationId() {
        val result = "example".createCorrelationId()
        println(result)
        assertNotNull(result)
        assertEquals(32, result.length)
    }

    @Test
    fun isBlankOrTextualNull() {
        assertTrue("".isBlankOrTextualNull())
        assertTrue("null".isBlankOrTextualNull())
    }
}