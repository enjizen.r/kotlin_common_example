package com.wanchalerm.tua.common.extension

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class ValidationExtensionKtTest {

    @Test
    fun `is valid email success`() {
        val result = "enjize@gmail.com".isValidEmail()
        assertTrue(result)
    }

    @Test
    fun `is valid email failed`() {
        val result = "enjize.gmail.com".isValidEmail()
        assertFalse(result)
    }

    @Test
    fun `isValid MobileNumber success`() {
        val result = "0917804665".isValidMobileNumber()
        assertTrue(result)
    }

    @Test
    fun `isValidMobileNumber null`() {
        val mobileNumber: String? = null
        val result = mobileNumber?.isValidMobileNumber() == true
        assertFalse(result)
    }
}