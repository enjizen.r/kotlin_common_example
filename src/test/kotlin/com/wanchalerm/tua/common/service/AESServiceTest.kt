package com.wanchalerm.tua.common.service

import com.wanchalerm.tua.common.extension.getPrivateKeyFromBase64
import com.wanchalerm.tua.common.extension.getPublicKeyFromBase64
import org.junit.jupiter.api.Test
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.SecureRandom
import java.util.Base64
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey


class AESServiceTest {

    @Test
    fun encrypt() {
        println("AES encrypt")
        val aesKey = generateKey()
        val iv = generateIv()

        val aesKeyString = Base64.getEncoder().encodeToString(aesKey.encoded)
        val ivString = Base64.getEncoder().encodeToString(iv)
        val aesService = AESService()
        val ae = aesService.encrypt("""
            {
                "first_name":"วันเฉลิม",
                "last_name":"ภูมิเลิศ",
                "birth_date":"1987-04-12",
                "mobile_number":"0919890287",
                "email":"enjizen.r@gmail.com",
                "password":"123456"
            }
        """.trimIndent(), aesKeyString, ivString)
        println(ae)
        //val ad = aesService.decrypt(ae,  Base64.getEncoder().encodeToString(aesKey.encoded), Base64.getEncoder().encodeToString(iv))
        println("AES Key: $aesKeyString")
        println("Iv: $ivString")
        println(ae)

        println("rsa encrypted aes key: ${encryptRsaKey(aesKeyString, ivString)}")

       // println("RSA encryption")

       /* val rsaKey = generateRSAKeyPair()
        println("public : ${Base64.getEncoder().encodeToString(rsaKey.public.encoded)}")
        println("private : ${Base64.getEncoder().encodeToString(rsaKey.private.encoded)}")

        val r = RSAService()
        val en = r.encryptData("data", rsaKey.public)
        println(en)
        val de = r.decryptData(en, rsaKey.private)
        println(de)*/


    }

    @Test
    fun aesEncrypt() {
        val aesService = AESService()
        val ae = aesService.encrypt("""
            {
                "first_name":"วันเฉลิม",
                "last_name":"ภูมิเลิศ",
                "birth_date":"1987-04-12",
                "mobile_number":"0919890217",
                "email":"enjizen2.r@gmail.com",
                "password":"123456"
            }
        """.trimIndent(), "VqKgE555v5UN5L8+NkTgpeY+fNk74+LjkFo7vGKlIQs=", "mrIZeYop37YAsdEv")

        println(ae)
    }

    @Test
    fun aesDecode() {
       /* AES Key: VqKgE555v5UN5L8+NkTgpeY+fNk74+LjkFo7vGKlIQs=
            Iv: mrIZeYop37YAsdEv */

        val data = "PJkvEvs7KmHjg+A3DN6AeMmTBo/fiXnD5YWfsnQYUB6HohmtIU763OO/ffPOGFIGJ2iPKVWA6rvS5qr6z3nT9sMCcCs0/f2uF14ZJxff5E1uJ4ZmmAL+ts28skkArgVXXYcKux8XasXIrIH3m8SCqGrLzHuWtsPzHmKlRosIUKN9EYNn1TDHQRqu/II5DKRiNomFyOs5HedHIc65UWfHl5NUg+wr1x8OLX9P/kgilnvjTEeNRaD2sRgObgUc6uzNwq3l4l+TrTJptyhV78DiV5PUIwLWnnN80dpZeQSUdZJpYZ9aG5hVGxJ8CD+YCGeZ77iGfqc7PZdJ3I0YrE5XmgQPMJnSceNarOhoHkXTkDHOwmmiscXJEHVUEDkUh5/R3BXjJrGe7ZWTcHNHculn5N5c05X9xBYPXgNu+oIDESc5r7CER89xYeEqjaV0Z++gh6BKjZct2Q2K49I5yrwskBFj"

        val aesService = AESService()
        val result = aesService.decrypt(data, "vnlXX1uk38Yz+/MEMqy4grWBbndPDbWpVdtjIpWVfkY=", "8Rkv0cFYVzHjWxp9")
        println(result)
    }

    @Test
    fun rsa() {
        val publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0cV+L/jhpjxBk5I7QxWjQHMsQjTaxC0LKIUGTOCMeR6RBk4fqKfpqmoTP8XYIclaYwWmCQQCgj/gwBGZaFhWSLh0BmFDcn+EK/cRWWEf3xlCf3oIrkn1Bpl4fhhvtAdX9GNQQZB4C+qbhgK+T/1Ux3HoXXirs/lozGqiIrFcaoIY13GjqFkmoSMfWz3bmBy7MtqU4SEUF9NZpDZLoogKQFwoqRhENZWuc0htDT9PEuqJTHGC/TwbaydAWqwxntFKrKzZtoHAPV6REKn55uJNjt7eaK157ICHadrc7tFPpVR7W4gALkNmudJR5bQcVssd9/c0CgRTdywO6l8aFA6f+wIDAQAB".getPublicKeyFromBase64()
        val data = "lmJSVMB5ozJmVefoZZjHo1xx1oSLVIOmZkyfrP6pFJo=|XKRP9vEFEyhI9R1R"


        val message = RSAService().encryptData(data, publicKey)
        println(message)
    }

    private fun encryptRsaKey(aesKey: String, iv: String): String {
        val publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0cV+L/jhpjxBk5I7QxWjQHMsQjTaxC0LKIUGTOCMeR6RBk4fqKfpqmoTP8XYIclaYwWmCQQCgj/gwBGZaFhWSLh0BmFDcn+EK/cRWWEf3xlCf3oIrkn1Bpl4fhhvtAdX9GNQQZB4C+qbhgK+T/1Ux3HoXXirs/lozGqiIrFcaoIY13GjqFkmoSMfWz3bmBy7MtqU4SEUF9NZpDZLoogKQFwoqRhENZWuc0htDT9PEuqJTHGC/TwbaydAWqwxntFKrKzZtoHAPV6REKn55uJNjt7eaK157ICHadrc7tFPpVR7W4gALkNmudJR5bQcVssd9/c0CgRTdywO6l8aFA6f+wIDAQAB".getPublicKeyFromBase64()
        val data = "$aesKey|$iv"
        return RSAService().encryptData(data, publicKey)
        //println(message)
    }

    @Test
    fun dersa() {
        val publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0cV+L/jhpjxBk5I7QxWjQHMsQjTaxC0LKIUGTOCMeR6RBk4fqKfpqmoTP8XYIclaYwWmCQQCgj/gwBGZaFhWSLh0BmFDcn+EK/cRWWEf3xlCf3oIrkn1Bpl4fhhvtAdX9GNQQZB4C+qbhgK+T/1Ux3HoXXirs/lozGqiIrFcaoIY13GjqFkmoSMfWz3bmBy7MtqU4SEUF9NZpDZLoogKQFwoqRhENZWuc0htDT9PEuqJTHGC/TwbaydAWqwxntFKrKzZtoHAPV6REKn55uJNjt7eaK157ICHadrc7tFPpVR7W4gALkNmudJR5bQcVssd9/c0CgRTdywO6l8aFA6f+wIDAQAB".getPublicKeyFromBase64()
        val data = "VqKgE555v5UN5L8+NkTgpeY+fNk74+LjkFo7vGKlIQs=|mrIZeYop37YAsdEv"


        val message = RSAService().decryptData("DKqVKSjiYmgv/dMiDRF36QhOzDCh4sqQQhUbFOXNR8+0Ae1PF1W9dYWbNlFFOUN+rXSmq3stJaZIXZaOKjRcdutoJ2x72EVdQp6hBj8tdmO/LYQgkK65ja4mNXJhNeRF3G/kwoKZcC5rC5urk+8b/z3J95y7Ne7m7twVLIubmaF9hYiMlb9MNbsllXTHf2PMO6cZ1L+yAzPZ5D0RPIu70+aJmdBOEaYjQKBs71XySphy1Us0TFjm/yveg+WK6su++W/TUbOEX9G14rfE2nMfr4mikWeHxtILGna2v7wxXPffeORRpyQqkskMO1qLO8joXla+kIZROk8Bu4YLa4zU1A==",
            "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDRxX4v+OGmPEGTkjtDFaNAcyxCNNrELQsohQZM4Ix5HpEGTh+op+mqahM/xdghyVpjBaYJBAKCP+DAEZloWFZIuHQGYUNyf4Qr9xFZYR/fGUJ/egiuSfUGmXh+GG+0B1f0Y1BBkHgL6puGAr5P/VTHcehdeKuz+WjMaqIisVxqghjXcaOoWSahIx9bPduYHLsy2pThIRQX01mkNkuiiApAXCipGEQ1la5zSG0NP08S6olMcYL9PBtrJ0BarDGe0UqsrNm2gcA9XpEQqfnm4k2O3t5orXnsgIdp2tzu0U+lVHtbiAAuQ2a50lHltBxWyx339zQKBFN3LA7qXxoUDp/7AgMBAAECggEAIhXeKDtfpxxZZ3o2aPMmIL3WLjfaaFANZRsgZ/OAv9mIpSainEwUet1bnVwvf2Xxau+rCAH/ylxIz8zMSZk5A/381GCPjhZMSryNtCFFBYJcPMx+hpt71gOwAYVE+VLMJujQTKcF6qHHfaIKfILkeLf4kt+dvtPDBJw7FjoNkzyfLJr6/ufpKhr2lNomjXRmGqn6aZYN2nXvrOyZZT9ZZVSjf8mBsNbHEXkS24/6C7v6GuIPB8hM3YTlPEaGlqLtU9mqaZkxUnMZ1UE6JpBs8XbfO9IrRt8c36dcwM6kW5CN7xK+U4qmcnSON2MJ16OL9+t3NuOZfPhUvMmwgZRFiQKBgQDj07EHVmgr1WtbrzStk1FBCl1eIK1lnGyjmOu4UhW+CG2ndzmTyAeDnm7qstZrW7PAd9nZqDDeXL85LAE2Jgv0YWW0kKU3bFeIL72+o2uh1ZTbgdZvKmHxr04psREtuBhWjyl2xVGvPkNch3xijqDrzJGuSKYzMqzvKgEnb4SypwKBgQDrtjhi9ryMEWf71V3i518LTx5+wvDYqNduN2v7rL3Qu9AxzhrA0JaPNzeX1Ff5kyda094HL/2XpIRH3oyG8mZhdDGfhztyDzVUHVuAhvpApzEKjcNHaOX6LDRFnZZ3sh+GPZrFW781l84edBZ5H182lOWaeBlKfehzEhFsCY82jQKBgGL8BLxFZHk2/rAVDXQkv3bcw/gzhmLRdboWpMOulle8XbgW6uM0U19yJIrALLz0WMLUz8frdWAHN+XlIpmeKi+Baajr93x/DADVjUEYy5jCrSw52yrtZSSWoAUpM2eDxr9E8CT0UisriCTZkMdZZL1OYjUwx1UTPWOkAmBZ6XOdAoGAJqY4VLHUdLsd68Muo9gAU6BjhbF9QGle+amIwTmv3hwNpT0AUgFhzpXlPbYJvtiMKW54B4Gs+UJ2ljo9nJr+gphE0r2f17H6spIlwMthGVzQw8Q+8uSWYRECeS+/zlycoqxTRm0myyjbGU87qBymxhJgG5X5v6cpiES8KpTesLkCgYAzPh57P7okCv/WhV+xG5EBo8DW7SotDB6GCZVIqCeirOmHyf9R9Z6y9J6CMerQrCewvdgsMBmGAy4j7c6XQ3fCGc7DPqQFSGmbV3F6PoHWqdNiieks+79n2cp6oV4AcpGnNejQeLOfBJuszcx2+T9xwLzFsTucDRGzV+42bFYwNw==".getPrivateKeyFromBase64())
        println(message)
    }

    fun generateKey(): SecretKey {
        val keyGen = KeyGenerator.getInstance("AES")
        keyGen.init(256)
        return keyGen.generateKey()
    }

    fun generateIv(): ByteArray {
        val iv = ByteArray(12)
        SecureRandom().nextBytes(iv)
        return iv
    }


    fun generateRSAKeyPair(): KeyPair {
        val keyPairGenerator = KeyPairGenerator.getInstance("RSA")
        keyPairGenerator.initialize(2048) // You can adjust key size as needed (2048-bit is common)
        return keyPairGenerator.generateKeyPair()
    }
}